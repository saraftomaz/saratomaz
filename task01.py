#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" This script calculates the quality stats from LST products"""

import giosystemcore.settings
import giosystemcore.files
import tables as t
import datetime as dt
import sys
import os
import argparse
from giosystemcore.catalogue.cswinterface import CswInterface 
from giosystemcore.utilities import get_unofficial_creation_date

def qualitystats(prodtile,timeslot):

    product = 'LST'
    
    #number of land pixels by tile
    if prodtile == 'lst african tile':
        numberOfLandPixels = 1236168
        tile = 'AFRI'
    elif prodtile == 'lst european tile':
        numberOfLandPixels = 474073
        tile = 'EURO'
    elif prodtile == 'lst asian tile':
        numberOfLandPixels = 698191
        tile = 'ASIA'
    elif prodtile == 'lst oceanian tile':
        numberOfLandPixels = 350087
        tile = 'OCEA'
    elif prodtile == 'lst north american tile':
        numberOfLandPixels = 680186
        tile = 'NOAM'
    elif prodtile == 'lst south american tile':
        numberOfLandPixels = 654264
        tile = 'SOAM'
    else:
        print 'INVALID PRODUCT/TILE'
        sys.exit()
    
    # import image
    giosystemcore.settings.get_settings("http://gio-gl3.meteo.pt/giosystem/coresettings/api/v1/")
    f = giosystemcore.files.get_file(prodtile, timeslot)
    hdf5=f.fetch("/home/user/test_data/giosystem/data/work", use_archive=True)  
    image_size = os.path.getsize(hdf5)
    
    # open image, datasets and metadata
    op = t.open_file(hdf5)
    lst=op.root.LST
    mv = lst._v_attrs["MISSING_VALUE"]
    sf=lst._v_attrs["SCALING_FACTOR"]
    qflags = op.root.Q_FLAGS

    # LST matrix manipulation
    m64=lst[:].astype(t.numpy.float64)
    m64[m64==float(mv)]=t.numpy.nan
    mf = m64 / float(sf)

    # stats functions 
    avg,std,vmax,vmin = var_stats(mf)
    q1,q2,q3,en,ep = percentile_box(mf)
    perop,peron,outmax,outmin = outliers_stats(mf,numberOfLandPixels)
    procPix = proc_pixels(qflags,numberOfLandPixels)
    av_date = date(product,timeslot,tile)

def var_stats(mf):

    average = t.numpy.nanmean(mf)
    std = t.numpy.nanstd(mf)
    maximum = t.numpy.nanmax(mf)
    minimum = t.numpy.nanmin(mf)

    return average, std, maximum, minimum
    
    
def percentile_box(mf):

    n_nan = mf[~t.numpy.isnan(mf)]
    q1 = t.numpy.percentile(n_nan, 25) 
    q2 = t.numpy.percentile(n_nan, 50) 
    q3 = t.numpy.percentile(n_nan, 75) 
    en = t.numpy.percentile(n_nan, 5) 
    ep = t.numpy.percentile(n_nan, 95) 
    
    return q1,q2,q3,en,ep


def outliers_stats(mf,numberOfLandPixels):

    outmax = 80.5 
    outmin = -70.5 
    
    # outliers 
    op = (mf > outmax) 
    on = (mf < outmin) 
    perop = 100. * t.numpy.sum(op) / numberOfLandPixels
    peron = 100. * t.numpy.sum(on) / numberOfLandPixels

    mfmask = t.numpy.ma.array(mf,mask=op)
    mfNout = t.numpy.ma.array(mfmask,mask=on)
    
    # max and min values without outliers
    vmax = t.numpy.nanmax(mfNout)
    vmin = t.numpy.nanmin(mfNout)
    
    return perop,peron,vmax,vmin
    

def proc_pixels(qflags,numberOfLandPixels):

    clearMask   = (qflags[:]>>2 == 1)
    contMask    = (qflags[:]>>2 == 2)
    cloudMask   = (qflags[:]>>2 == 3)
    procPix     = 100.0 * t.numpy.sum( (cloudMask) | (clearMask) | (contMask) ) / numberOfLandPixels
    
    return procPix

def date(product,timeslot,tile):

    csw = CswInterface()
    #ids = csw.find_records_by_pattern("%LST%201502191300%EURO%")
    ids = csw.find_records_by_pattern('%%''%s''%%''%s''%%''%s''%%' % (product,timeslot,tile))
    product_date = dt.datetime.strptime(timeslot,"%Y%m%d%H%M%S")
    publication_date = get_unofficial_creation_date(ids[0], "http://geoland2.meteo.pt/giosystem/cataloguehacks/api/v1")
    t = publication_date-product_date
    av_time=t.seconds/3600.
    
    print ('product_date: %s' %product_date)
    print ('publication_date: %s' %publication_date)
    print ('available time: %s' %av_time)
    
    return av_time
   

if __name__ == "__main__":  # we are running on the command line
  
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )                        
    parser.add_argument('product_tile', help='Name of the product and name of the tile. '
                        'It can be one of: lst african tile, lst european tile, lst asian tile,'
                        'lst oceanian tile, lst north american tile, lst south american tile')                                
    parser.add_argument('timeslot', help='Format is YYYYMMDDhhmm')
    args = parser.parse_args()
    try:
        qualitystats(args.product_tile, args.timeslot)
        print ('\033[92m'+'Quality stats completed'+'\033[0m')
    except:
        print ('\033[91m'+'Unavailable file'+'\033[0m')
        sys.exit()


    

